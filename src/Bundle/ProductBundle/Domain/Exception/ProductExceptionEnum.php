<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Domain\Exception;

class ProductExceptionEnum
{
    const PRODUCT_NOT_FOUND = 'Product not found';
    const PRODUCT_NAME_BAD_REQUEST = 'Bad request - name invalid';
    const PRODUCT_PUBLIC_ID_BAD_REQUEST = 'Bad request - public_id invalid';
    const PRODUCT_PRICE_BAD_REQUEST = 'Bad request - price invalid';
}
