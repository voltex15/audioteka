<?php

namespace App\Bundle\ProductBundle\DataFixtures\Dev;

use App\Bundle\ProductBundle\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    private const COUNT = 20;

    public function __construct()
    {}

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            $product = new Product();
            $product->setName('Product ' . $i);
            $product->setPrice(mt_rand(1, 100));
            $product->setCreatedAt(new \DateTime('now'));
            $manager->persist($product);
        }

        $manager->flush();
    }
}