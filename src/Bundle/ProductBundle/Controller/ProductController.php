<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Controller;

use App\Bundle\ProductBundle\DTO\Product\CreateProductDTO;
use App\Bundle\ProductBundle\DTO\Product\UpdateProductDTO;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Bundle\ProductBundle\Application\Manager\ProductManager;
use App\Bundle\ProductBundle\Entity\Product;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[
    OA\Tag(name: 'Product'),
    OA\Response(
        response: 404,
        description: 'Resource not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'code', description: 'Error code', type: 'integer'),
                new OA\Property(property: 'message', description: 'Error description', type: 'string'),
            ],
            type: 'object'
        )
    )
]
class ProductController
{
    public function __construct(
        private ProductManager $manager
    ) {}

    #[
        Rest\Get("/product/{id}"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['productGet']
        ),
        OA\Parameter(
            name: 'id',
            in: 'path',
            required: true,
            schema: new OA\Schema(type: 'string')
        ),
        OA\Response(
            response: 200,
            description: 'Returns product object',
            content: new OA\JsonContent(
                ref: new Model(type: Product::class, groups: ['productGet']),

            )
        )
    ]
    public function getProductById(string $id)
    {
        return $this->manager->getById($id);
    }

    #[
        Rest\Get("/products"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['productGet']
        ),
        OA\Response(
            response: 200,
            description: 'Returns product list',
            content: new OA\JsonContent(
                type: 'array',
                items: new OA\Items(ref: new Model(type: Product::class, groups: ['productGet'])),
            )
        )
    ]
    public function getProducts()
    {
        return $this->manager->getList();
    }

    #[
        Rest\Post("/product"),
        Rest\View(
            statusCode: 201,
            serializerGroups: ['productCreate', 'productGet']
        ),
        ParamConverter("request", converter: "fos_rest.request_body"),
        OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: CreateProductDTO::class, groups: ['productCreate']),
            )
        ),
        OA\Response(
            response: 201,
            description: 'Returns created product object',
            content: new OA\JsonContent(
                ref: new Model(type: Product::class, groups: ['productCreate', 'productGet']),
            )
        )
    ]
    public function createProduct(CreateProductDTO $request)
    {
        return $this->manager->create($request);
    }

    #[
        Rest\Put("/product/{id}"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['productUpdate', 'productGet']
        ),
        ParamConverter("request", converter: "fos_rest.request_body"),
        OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: UpdateProductDTO::class, groups: ['productUpdate']),
            )
        ),
        OA\Response(
            response: 200,
            description: 'Returns created product object',
            content: new OA\JsonContent(
                ref: new Model(type: Product::class, groups: ['productUpdate', 'productGet']),
            )
        )
    ]
    public function updateProduct(string $id, UpdateProductDTO $request)
    {
        return $this->manager->update($id, $request);
    }

    #[
        Rest\Delete("/product/{id}"),
        Rest\View(
            statusCode: 204
        ),
        OA\Response(
            response: 204,
            description: 'Item deleted successfully',
        )
    ]
    public function deleteProduct(string $id): void
    {
        $this->manager->delete($id);
    }
}
