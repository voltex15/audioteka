<?php

namespace App\Bundle\ProductBundle\DTO\Product;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

abstract class ProductDTO
{
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Serializer\Groups(['orderCreate', 'orderGet'])]
    public ?string $id;

    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Serializer\Groups(['productCreate', 'productUpdate', 'orderCreate', 'orderGet'])]
    public ?string $name;

    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    #[Serializer\Groups(['productCreate', 'productUpdate', 'orderCreate', 'orderGet'])]
    public ?int $price;
}