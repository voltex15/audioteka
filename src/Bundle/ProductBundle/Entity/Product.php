<?php

namespace App\Bundle\ProductBundle\Entity;

use App\Bundle\OrderBundle\Entity\Order;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ORM\Table(name: 'product')]
class Product
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Serializer\Groups(['productGet', 'orderCreate', 'orderGet'])]
    private string $id;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Assert\Length(max: 255)]
    #[Serializer\Groups(['productCreate', 'productUpdate', 'productGet', 'orderCreate', 'orderGet'])]
    private string $name;

    #[ORM\Column(type: 'integer', nullable: false)]
    #[Serializer\Groups(['productCreate', 'productUpdate', 'productGet', 'orderCreate', 'orderGet'])]
    private string $price;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Serializer\Groups(['productGet', 'orderGet'])]
    private ?\DateTime $createdAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Serializer\Groups(['productGet', 'orderGet'])]
    private ?\DateTime $updatedAt;

    #[ORM\ManyToMany(targetEntity: Order::class, mappedBy: "products", cascade: ['persist', 'merge'])]
    #[Serializer\Exclude]
    private Collection $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function setOrders(ArrayCollection $orders): self
    {
        /** @var Order $order */
        foreach ($this->orders as $order) {
            if (!$orders->contains($order)) {
                $this->removeOrder($order);
            }
        }

        foreach ($orders as $order) {
            $this->addOrder($order);
        }

        return $this;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->addProduct($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            $order->removeProduct($this);
        }

        return $this;
    }
}
