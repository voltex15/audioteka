<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Factory;

use App\Bundle\ProductBundle\DTO\Product\CreateProductDTO;
use App\Bundle\ProductBundle\DTO\Product\UpdateProductDTO;
use App\Bundle\ProductBundle\Entity\Product;

class ProductFactory
{
    public function __construct()
    {}

    public function createFromRequest(Product $product, CreateProductDTO $request): Product
    {
        $product
            ->setName($request->name)
            ->setPrice($request->price)
            ->setCreatedAt(new \DateTime('now'))
        ;

        return $product;
    }

    public function updateFromRequest(Product $product, UpdateProductDTO $request): Product
    {
        $product
            ->setName($request->name)
            ->setPrice($request->price)
            ->setUpdatedAt(new \DateTime())
        ;

        return $product;
    }
}
