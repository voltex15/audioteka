<?php

namespace App\Bundle\ProductBundle\Repository;

use App\Bundle\ProductBundle\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function create(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    public function update(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }

    public function delete(Product $product)
    {
        $this->_em->remove($product);
        $this->_em->flush();
    }

    public function persist(Product $product): void
    {
        $this->_em->persist($product);
    }

    public function findOneById(string $id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
