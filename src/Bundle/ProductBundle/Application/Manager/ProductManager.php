<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Manager;

use App\Bundle\CoreBundle\Application\Manager\BaseManager;
use App\Bundle\ProductBundle\Application\Command\Product\Create\CreateCommand;
use App\Bundle\ProductBundle\Application\Command\Product\Delete\DeleteCommand;
use App\Bundle\ProductBundle\Application\Command\Product\Update\UpdateCommand;
use App\Bundle\ProductBundle\Application\Query\Product\GetById\GetByIdQuery;
use App\Bundle\ProductBundle\Application\Query\Product\GetList\GetListQuery;
use App\Bundle\ProductBundle\DTO\Product\CreateProductDTO;
use App\Bundle\ProductBundle\DTO\Product\UpdateProductDTO;
use Symfony\Component\Messenger\MessageBusInterface;

class ProductManager extends BaseManager
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function create(CreateProductDTO $request)
    {
        return $this->handle(
            new CreateCommand($request)
        );
    }

    public function update(string $id, UpdateProductDTO $request)
    {
        return $this->handle(
            new UpdateCommand($id, $request)
        );
    }

    public function delete(string $id)
    {
        return $this->handle(
            new DeleteCommand($id)
        );
    }

    public function getById(string $id)
    {
        return $this->handle(
            new GetByIdQuery($id)
        );
    }

    public function getList()
    {
        return $this->handle(
            new GetListQuery()
        );
    }
}
