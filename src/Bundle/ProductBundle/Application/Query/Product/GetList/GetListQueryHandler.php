<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Query\Product\GetList;

use App\Bundle\ProductBundle\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetListQueryHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly ProductRepository $productRepository
    ) {}

    public function __invoke(GetListQuery $query)
    {
        return $this->productRepository->findAll();
    }
}