<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Query\Product\GetById;

use App\Bundle\CoreBundle\Exception\NotFoundException;
use App\Bundle\ProductBundle\Domain\Exception\ProductExceptionEnum;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetByIdQueryHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly ProductRepository $productRepository
    ) {}

    public function __invoke(GetByIdQuery $query)
    {
        $product = $this->productRepository->findOneById($query->getId());
        if (!$product) {
            throw new NotFoundException(ProductExceptionEnum::PRODUCT_NOT_FOUND);
        }

        return $product;
    }
}