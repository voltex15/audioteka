<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Delete;

final class DeleteCommand
{
    public function __construct(
        private string $id
    ) {}

    public function getId(): string
    {
        return $this->id;
    }
}
