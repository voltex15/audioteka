<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Delete;

use App\Bundle\CoreBundle\Exception\NotFoundException;
use App\Bundle\ProductBundle\Domain\Exception\ProductExceptionEnum;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteCommandHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly ProductRepository $productRepository,
    ) {}

    public function __invoke(DeleteCommand $command)
    {
        $product = $this->productRepository->findOneById($command->getId());

        if (!$product) {
            throw new NotFoundException(ProductExceptionEnum::PRODUCT_NOT_FOUND);
        }

        try {
            $this->productRepository->delete($product);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
