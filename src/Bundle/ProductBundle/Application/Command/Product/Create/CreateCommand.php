<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Create;

use App\Bundle\ProductBundle\DTO\Product\CreateProductDTO;

final class CreateCommand
{
    public function __construct(
        private CreateProductDTO $request
    ) {}

    public function getRequest(): CreateProductDTO
    {
        return $this->request;
    }
}
