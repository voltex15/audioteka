<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Create;

use App\Bundle\ProductBundle\Entity\Product;
use App\Bundle\ProductBundle\Factory\ProductFactory;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateCommandHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly ProductRepository $productRepository,
        private readonly ProductFactory $productFactory,
    ) {}

    public function __invoke(CreateCommand $command)
    {
        try {
            $product = new Product();
            $product = $this->productFactory->createFromRequest($product, $command->getRequest());
            $this->productRepository->create($product);
        } catch (\Exception $e) {
            throw $e;
        }

        return $product;
    }
}
