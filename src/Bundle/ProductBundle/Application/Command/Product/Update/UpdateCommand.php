<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Update;

use App\Bundle\ProductBundle\DTO\Product\UpdateProductDTO;

final class UpdateCommand
{
    public function __construct(
        private string $id,
        private UpdateProductDTO $request
    ) {}

    public function getId(): string
    {
        return $this->id;
    }

    public function getRequest(): UpdateProductDTO
    {
        return $this->request;
    }
}
