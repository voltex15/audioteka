<?php declare(strict_types=1);

namespace App\Bundle\ProductBundle\Application\Command\Product\Update;

use App\Bundle\CoreBundle\Exception\NotFoundException;
use App\Bundle\ProductBundle\Domain\Exception\ProductExceptionEnum;
use App\Bundle\ProductBundle\Factory\ProductFactory;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateCommandHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly ProductRepository $productRepository,
        private readonly ProductFactory $productFactory,
    ) {}

    public function __invoke(UpdateCommand $command)
    {
        $product = $this->productRepository->findOneById($command->getId());

        if (!$product) {
            throw new NotFoundException(ProductExceptionEnum::PRODUCT_NOT_FOUND);
        }

        try {
            $product = $this->productFactory->updateFromRequest($product, $command->getRequest());
            $this->productRepository->update($product);
        } catch (\Exception $e) {
            throw $e;
        }

        return $product;
    }
}
