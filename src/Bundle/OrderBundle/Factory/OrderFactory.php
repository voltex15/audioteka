<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Factory;

use App\Bundle\CoreBundle\Exception\NotFoundException;
use App\Bundle\OrderBundle\DTO\Order\CreateOrderDTO;
use App\Bundle\OrderBundle\Entity\Order;
use App\Bundle\ProductBundle\Domain\Exception\ProductExceptionEnum;
use App\Bundle\ProductBundle\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;

class OrderFactory
{
    public function __construct(
        private readonly ProductRepository $productRepository
    )
    {}

    public function createFromRequest(Order $order, CreateOrderDTO $request): Order
    {
        $products = $this->prepareProducts($request->products);

        $order->setCreatedAt(new \DateTime('now'));
        $order->setProducts($products);

        return $order;
    }

    private function prepareProducts(ArrayCollection $productsDTO): ArrayCollection
    {
        $products = new ArrayCollection();
        foreach ($productsDTO as $product) {
            $productEntity = $this->productRepository->findOneById($product->id);

            if (!$productEntity) {
                throw new NotFoundException(ProductExceptionEnum::PRODUCT_NOT_FOUND);
            }
            $products[] = $productEntity;
        }
        return $products;
    }
}
