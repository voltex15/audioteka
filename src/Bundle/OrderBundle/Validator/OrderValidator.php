<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Validator;

use App\Bundle\CoreBundle\Validator\ValidatorInterface;
use App\Bundle\OrderBundle\DTO\Order\CreateOrderDTO;
use Doctrine\Common\Collections\ArrayCollection;

class OrderValidator implements ValidatorInterface
{
    private const COUNT = 3;
    private CreateOrderDTO $order;

    public function __construct()
    {}

    public function setOrder(CreateOrderDTO $order)
    {
        $this->order = $order;
    }

    public function validate(): bool
    {
        return $this->validateCount($this->order->products);
    }

    private function validateCount(ArrayCollection $products): bool
    {
        if ($products->count() > self::COUNT)
        {
            return false;
        }
        return true;
    }
}
