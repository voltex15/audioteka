<?php

namespace App\Bundle\OrderBundle\Entity;

use App\Bundle\OrderBundle\Repository\OrderRepository;
use App\Bundle\ProductBundle\Entity\Product;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    #[Serializer\Groups(['productGet', 'orderGet', 'orderCreate'])]
    private string $id;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Serializer\Groups(['productGet', 'orderGet', 'orderCreate'])]
    private ?\DateTime $createdAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Serializer\Groups(['productGet', 'orderGet', 'orderCreate'])]
    private ?\DateTime $updatedAt;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: "orders", cascade: ['persist'])]
    #[ORM\JoinTable(name: "order_products",
        joinColumns: [new ORM\JoinColumn(name: "order_id", referencedColumnName: "id")],
        inverseJoinColumns: [new ORM\JoinColumn(name: "product_id", referencedColumnName: "id")]
    )]
    #[Serializer\Groups(['orderCreate', 'orderUpdate', 'orderGet'])]
    private Collection $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts(ArrayCollection $products): self
    {
        /** @var Product $product */
        foreach ($this->products as $product) {
            if (!$products->contains($product)) {
                $this->removeProduct($product);
            }
        }

        $this->products = $products;

        return $this;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addOrder($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }

        return $this;
    }
}
