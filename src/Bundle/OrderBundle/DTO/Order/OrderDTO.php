<?php

namespace App\Bundle\OrderBundle\DTO\Order;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\ArrayCollection;

abstract class OrderDTO
{
    #[Assert\Valid]
    #[Serializer\Groups(['tableCreate', 'tableUpdate'])]
    #[Serializer\Type('ArrayCollection<App\Bundle\ProductBundle\DTO\Product\CreateProductDTO>')]
    public ?ArrayCollection $products;
}