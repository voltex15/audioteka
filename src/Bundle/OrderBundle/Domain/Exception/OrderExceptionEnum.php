<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Domain\Exception;

class OrderExceptionEnum
{
    const ORDER_NOT_FOUND = 'Product not found';
    const ORDER_HAS_TOO_MANY_PRODUCTS = 'Order has too many products';
}
