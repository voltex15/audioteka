<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Controller;

use App\Bundle\OrderBundle\Application\Manager\OrderManager;
use App\Bundle\OrderBundle\DTO\Order\CreateOrderDTO;
use App\Bundle\OrderBundle\Entity\Order;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[
    OA\Tag(name: 'Order'),
    OA\Response(
        response: 404,
        description: 'Resource not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'code', description: 'Error code', type: 'integer'),
                new OA\Property(property: 'message', description: 'Error description', type: 'string'),
            ],
            type: 'object'
        )
    )
]
class OrderController
{
    public function __construct(
        private OrderManager $manager
    )
    {}

    #[
        Rest\Get("/order/{id}"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['orderGet']
        ),
        OA\Parameter(
            name: 'id',
            in: 'path',
            required: true,
            schema: new OA\Schema(type: 'string')
        ),
        OA\Response(
            response: 200,
            description: 'Returns order object',
            content: new OA\JsonContent(
                ref: new Model(type: Order::class, groups: ['orderGet']),

            )
        )
    ]
    public function getOrderById(string $id)
    {
        return $this->manager->getById($id);
    }

    #[
        Rest\Get("/orders"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['orderGet']
        ),
        OA\Response(
            response: 200,
            description: 'Returns order list',
            content: new OA\JsonContent(
                type: 'array',
                items: new OA\Items(ref: new Model(type: Order::class, groups: ['orderGet'])),
            )
        )
    ]
    public function getOrders()
    {
        return $this->manager->getList();
    }

    #[
        Rest\Post("/order"),
        Rest\View(
            statusCode: 201,
            serializerGroups: ['orderCreate', 'orderGet']
        ),
        ParamConverter("request", converter: "fos_rest.request_body"),
        OA\RequestBody(
            required: true,
            content: new OA\JsonContent(
                ref: new Model(type: CreateOrderDTO::class, groups: ['orderCreate']),
            )
        ),
        OA\Response(
            response: 201,
            description: 'Returns created order object',
            content: new OA\JsonContent(
                ref: new Model(type: Order::class, groups: ['orderCreate', 'orderGet']),
            )
        )
    ]
    public function createOrder(CreateOrderDTO $request)
    {
        return $this->manager->create($request);
    }
}
