<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Query\Order\GetList;

use App\Bundle\OrderBundle\Repository\OrderRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetListQueryHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly OrderRepository $orderRepository
    ) {}

    public function __invoke(GetListQuery $query)
    {
        return $this->orderRepository->findAll();
    }
}