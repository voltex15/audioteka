<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Query\Order\GetById;

use App\Bundle\CoreBundle\Exception\NotFoundException;
use App\Bundle\OrderBundle\Domain\Exception\OrderExceptionEnum;
use App\Bundle\OrderBundle\Repository\OrderRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class GetByIdQueryHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly OrderRepository $orderRepository
    ) {}

    public function __invoke(GetByIdQuery $query)
    {
        $order = $this->orderRepository->findOneById($query->getId());
        if (!$order) {
            throw new NotFoundException(OrderExceptionEnum::ORDER_NOT_FOUND);
        }

        return $order;
    }
}