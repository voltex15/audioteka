<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Query\Order\GetById;

final class GetByIdQuery
{
    public function __construct(private string $id)
    {}

    public function getId(): string
    {
        return $this->id;
    }
}
