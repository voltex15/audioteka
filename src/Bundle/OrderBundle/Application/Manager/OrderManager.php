<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Manager;

use App\Bundle\CoreBundle\Application\Manager\BaseManager;
use App\Bundle\OrderBundle\Application\Command\Order\Create\CreateCommand;
use App\Bundle\OrderBundle\Application\Query\Order\GetById\GetByIdQuery;
use App\Bundle\OrderBundle\Application\Query\Order\GetList\GetListQuery;
use App\Bundle\OrderBundle\DTO\Order\CreateOrderDTO;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderManager extends BaseManager
{
    public function __construct(MessageBusInterface $messageBus)
    {
        parent::__construct($messageBus);
    }

    public function create(CreateOrderDTO $request)
    {
        return $this->handle(
            new CreateCommand($request)
        );
    }

    public function getById(string $id)
    {
        return $this->handle(
            new GetByIdQuery($id)
        );
    }

    public function getList()
    {
        return $this->handle(
            new GetListQuery()
        );
    }
}
