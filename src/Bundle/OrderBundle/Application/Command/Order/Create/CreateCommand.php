<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Command\Order\Create;

use App\Bundle\OrderBundle\DTO\Order\CreateOrderDTO;

final class CreateCommand
{
    public function __construct(
        private CreateOrderDTO $request
    ) {}

    public function getRequest(): CreateOrderDTO
    {
        return $this->request;
    }
}
