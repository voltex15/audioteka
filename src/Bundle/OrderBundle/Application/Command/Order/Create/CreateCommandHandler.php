<?php declare(strict_types=1);

namespace App\Bundle\OrderBundle\Application\Command\Order\Create;

use App\Bundle\CoreBundle\Exception\BadRequestException;
use App\Bundle\OrderBundle\Domain\Exception\OrderExceptionEnum;
use App\Bundle\OrderBundle\Entity\Order;
use App\Bundle\OrderBundle\Factory\OrderFactory;
use App\Bundle\OrderBundle\Repository\OrderRepository;
use App\Bundle\OrderBundle\Validator\OrderValidator;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateCommandHandler implements MessageHandlerInterface
{
    public function __construct(
        private readonly OrderRepository $orderRepository,
        private readonly OrderFactory $orderFactory,
        private readonly OrderValidator $orderValidator,
    ) {}

    public function __invoke(CreateCommand $command)
    {
        $this->orderValidator->setOrder($command->getRequest());

        if (!$this->orderValidator->validate())
        {
            throw new BadRequestException(OrderExceptionEnum::ORDER_HAS_TOO_MANY_PRODUCTS);
        }

        try {
            $order = new Order();
            $order = $this->orderFactory->createFromRequest($order, $command->getRequest());
            $this->orderRepository->create($order);
        } catch (\Exception $e) {
            throw $e;
        }

        return $order;
    }
}
