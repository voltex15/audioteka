<?php

namespace App\Bundle\OrderBundle\Repository;

use App\Bundle\OrderBundle\Entity\Order;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function create(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function update(Order $order): void
    {
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function delete(Order $order)
    {
        $this->_em->remove($order);
        $this->_em->flush();
    }

    public function persist(Order $order): void
    {
        $this->_em->persist($order);
    }

    public function findOneById(string $id)
    {
        return $this->createQueryBuilder('o')
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
