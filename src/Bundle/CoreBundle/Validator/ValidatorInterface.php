<?php declare(strict_types=1);

namespace App\Bundle\CoreBundle\Validator;

interface ValidatorInterface
{
    public function validate();
}