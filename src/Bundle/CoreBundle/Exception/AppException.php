<?php declare(strict_types=1);

namespace App\Bundle\CoreBundle\Exception;

class AppException extends \Exception
{
    private string $appCode;
    private string $appMessage;
    private int $httpCode;

    public function __construct(string $appCode, int $httpCode)
    {
        parent::__construct();
        $this->appCode = $appCode;
        $this->httpCode = $httpCode;
    }

    public function getAppCode(): string
    {
        return $this->appCode;
    }

    public function getAppMessage(): string
    {
        return $this->appMessage;
    }

    public function setAppMessage(string $appMessage): void
    {
        $this->appMessage = $appMessage;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function setHttpCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }
}
