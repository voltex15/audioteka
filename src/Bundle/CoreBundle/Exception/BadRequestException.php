<?php declare(strict_types=1);

namespace App\Bundle\CoreBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

class BadRequestException extends AppException
{
    const HTTP_CODE = Response::HTTP_BAD_REQUEST;

    public function __construct(string $appCode, int $httpCode = self::HTTP_CODE)
    {
        parent::__construct($appCode, $httpCode);
    }
}
