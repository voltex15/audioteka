<?php

namespace App\Bundle\CoreBundle\Listener;

use App\Bundle\CoreBundle\Exception\AppException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function __construct() {}

    public function onKernelException(ExceptionEvent $event): void
    {
        if ($event->getThrowable() instanceof AppException) {
            $event->setResponse(
                new JsonResponse(
                    $event->getThrowable()->getPrevious()->getAppCode() ?: 'Undefined error',
                    $event->getThrowable()->getPrevious()->getHttpCode() ?: 400
                )
            );
        }
        else if ($event->getThrowable() instanceof \Exception)
        {
            $event->setResponse(
                new JsonResponse(
                    $event->getThrowable()->getPrevious()->getMessage(),
                    $event->getThrowable()->getPrevious()->getCode()
                )
            );
        }
    }
}
